# General-purpose PHP assessment for ClearView

PHP assessment task to cover OOP, API development, composer & docker savvy, tdd, etc.

## Assignment #1
### Object-oriented programming

Create a reusable and easily extendible PHP library for defining various shapes, and calculating their area, width and height.

Example in PHP:

```php
class Oval extends Shape
{
    public function getArea()
    {
        return (M_PI * $this->A / 2) * $this->B / 2;
    }
}
```

Use any design pattern you can think of to implement here. Accomplish a decent level of abstraction & polimorphysm.
Keep in mind that you're developing an API or a library, and publish this onto your GitHub, and later on use as a dependency.

**BONUS:** Unit & integration tests.

---

## Assignment #2
### Library implementation

Implement the library defined beforehand in any kind of a PHP project, framework, toolset.
Install the library as a `composer̨` dependency (do not publish your library on [packagist.org](https://packagist.org)!), and implement a compliant PSR auto-loader.

This assignment can be delivered as a CLI script, an HTTP script, a microservice, etc. All it has to do is to output calculation values for all the shapes that are implemented.

**BONUS:** Implement in Laravel or Symfony in a container.

---

## Assignment #3
### WordPress plugin

Create a WordPress plugin which will utilize your library and create tabs for each defined `Shape`, and inside each `Shape` tab create a form which will accept input values to define size and constraints of a given shape, and on submit of that particular form, you should present calculation result below the form, using the inputs user has provided.

**BONUS:** Use WordPress's native utility for generating tabs and forms.

---

## Important notes

Assignment #1 is mandatory, whilst the other two are optional, but if all three completed and delivered, that will be a great benefit to your job application.

Host your code on GitHub in private repositories, and grant us access to this GitHub user: @4050350194 - with which we'll conduct the code review and final evaluation.

### Other bonuses

1. Add other calculation and manipulation methods into shapes.
2. Decent `README.md` with description and instruction for all projects is a big bonus.
3. PHPDoc blocks are welcome, but unnecessary and redundant comments are not.
4. Code should be self-explanatory and properly formatted, along with proper and consistent naming convention.
5. A live demo with a publicly accessible URL will be a huge bonus (for either of the assignments).
6. Create a public REST API which will utilize the library but provide necessary `GET` and/or `POST` endpoints which will be accessed by solutions you provide for either assignment #2 or #3. 
7. Containerization and Dockerization, along with previous items, will give you the job immediatelly.
